# ROS bag play
rosbag play "bag_name" --pause

# Export ROS bag data to CSV
rostopic echo "topic_name" -b "bag_name" -p > "file.csv"

# Import to Matlab as a dataset



# To copy from remote to local
scp jlsanche@maemi-wifi:/home/jlsanche/workspace/ros/fuseon_ws_catkin/src/fuseon_stack/logs/session1_2016-02-03-16-09-57.bag ./
