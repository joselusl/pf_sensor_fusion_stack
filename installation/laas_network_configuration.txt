
# HOSTS

# elCidCampeador
140.93.65.112 elCidCampeador
# maemi
140.93.16.106 maemi
140.93.16.107 maemi-wifi



# EXPORT VARIABLES

# In maemi, over wifi
export ROS_HOSTNAME=140.93.16.107

# In elCidCampeador
export ROS_IP=140.93.65.112
