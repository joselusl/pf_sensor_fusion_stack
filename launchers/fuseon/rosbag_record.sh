#!/bin/bash

# sudo mkdir -p /media/joselusl/Elements
# sudo mount -t ntfs-3g -o uid=joselusl /dev/sda1 /media/joselusl/Elements
# sudo umount /media/joselusl/Elements

# cd /media/joselusl/Elements
# cd fuseon/logs


source $FUSEON_STACK/setup.sh fuseon_odroid_01


rosbag record -o session2 \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/camera_info \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/disparity \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/image_raw \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/points \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/rgb/camera_info \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/rgb/image_raw \
  /fuseon/hokuyo_lidar/scan \
  /fuseon/phidgets_gps_1040/gps/nav_sat_fix \
  /fuseon/phidgets_gps_1040/gps/nav_sat_status \
  /fuseon/phidgets_gps_1040/time_reference \
  /fuseon/phidgets_imu_1042/imu/data_raw \
  /fuseon/phidgets_imu_1042/is_calibrated \
  /fuseon/phidgets_imu_1042/mag/data_raw \
  /fuseon/phidgets_imu_1044/imu/data_raw \
  /fuseon/phidgets_imu_1044/is_calibrated \
  /fuseon/phidgets_imu_1044/mag/data_raw \
  /fuseon/ueye_camera_left/camera/camera_info \
  /fuseon/ueye_camera_left/camera/image_raw \
  /fuseon/ueye_camera_right/camera/camera_info \
  /fuseon/ueye_camera_right/camera/image_raw \
  /fuseon/usglobalsat_gps/gps/nav_sat_fix \
  /fuseon/usglobalsat_gps/gps/nav_sat_vel \
  /fuseon/usglobalsat_gps/gps/time_reference \
  /tf \
  /tf_static
