#!/bin/bash

# sudo mkdir -p /media/joselusl/Elements
# sudo mount -t ntfs-3g -o uid=joselusl /dev/sda1 /media/joselusl/Elements
# sudo umount /media/joselusl/Elements

# cd /media/joselusl/Elements
# cd fuseon/logs


source $FUSEON_STACK/setup.sh fuseon_odroid_01
cd $FUSEON_STACK/logs

rosbag record -o session2 \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/camera_info \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/disparity \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/image_raw \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/depth_registered/points \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/rgb/camera_info \
  /fuseon/asus_xtion_pro_live_rgbd_camera/asus_xtion_pro_live/rgb/image_raw
