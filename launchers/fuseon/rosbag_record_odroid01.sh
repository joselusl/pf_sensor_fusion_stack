#!/bin/bash

# sudo mkdir -p /media/joselusl/Elements
# sudo mount -t ntfs-3g -o uid=joselusl /dev/sda1 /media/joselusl/Elements
# sudo umount /media/joselusl/Elements

# cd /media/joselusl/Elements
# cd fuseon/logs


source $FUSEON_STACK/setup.sh fuseon_odroid_01
cd $FUSEON_STACK/logs

rosbag record -o session2 \
  /fuseon/hokuyo_lidar/scan \
  /fuseon/phidgets_imu_1042/imu/data_raw \
  /fuseon/phidgets_imu_1042/is_calibrated \
  /fuseon/phidgets_imu_1042/mag/data_raw \
  /fuseon/phidgets_imu_1044/imu/data_raw \
  /fuseon/phidgets_imu_1044/is_calibrated \
  /fuseon/phidgets_imu_1044/mag/data_raw
