#!/bin/bash

# sudo mkdir -p /media/joselusl/Elements
# sudo mount -t ntfs-3g -o uid=joselusl /dev/sda1 /media/joselusl/Elements
# sudo umount /media/joselusl/Elements

# cd /media/joselusl/Elements
# cd fuseon/logs


source $FUSEON_STACK/setup.sh fuseon_odroid_01
cd $FUSEON_STACK/logs

rosbag record -o session2 \
  /fuseon/phidgets_gps_1040/gps/nav_sat_fix \
  /fuseon/phidgets_gps_1040/gps/nav_sat_status \
  /fuseon/phidgets_gps_1040/time_reference \
  /fuseon/ueye_camera_right/camera/camera_info \
  /fuseon/ueye_camera_right/camera/image_raw

