#!/bin/bash

# sudo mkdir -p /media/joselusl/Elements
# sudo mount -t ntfs-3g -o uid=joselusl /dev/sda1 /media/joselusl/Elements
# sudo umount /media/joselusl/Elements

# cd /media/joselusl/Elements
# cd fuseon/logs


source $FUSEON_STACK/setup.sh fuseon_odroid_01
cd $FUSEON_STACK/logs

rosbag record -o session2 \
  /fuseon/ueye_camera_left/camera/camera_info \
  /fuseon/ueye_camera_left/camera/image_raw \
  /fuseon/usglobalsat_gps/gps/nav_sat_fix \
  /fuseon/usglobalsat_gps/gps/nav_sat_vel \
  /fuseon/usglobalsat_gps/gps/time_reference \
  /tf \
  /tf_static
