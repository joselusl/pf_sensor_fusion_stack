#!/bin/bash

# sudo mkdir -p /media/joselusl/Elements
# sudo mount -t ntfs-3g -o uid=joselusl /dev/sda1 /media/joselusl/Elements
# sudo umount /media/joselusl/Elements

# cd /media/joselusl/Elements
# cd fuseon/logs


cd $FUSEON_STACK/logs

rosbag record -o session1 \
  /fuseon/phidgets_imu_1044/imu/data_raw \
  /fuseon/phidgets_imu_1044/is_calibrated \
  /fuseon/phidgets_imu_1044/mag/data_raw \
  /fuseon/px4flow/px4flow/camera_image \
  /fuseon/px4flow/px4flow/opt_flow \
  /fuseon/ueye_camera/camera/camera_info \
  /fuseon/ueye_camera/camera/image_raw \
  /tkbridge/MSF
