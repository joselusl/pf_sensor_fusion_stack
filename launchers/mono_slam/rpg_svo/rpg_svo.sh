#!/bin/bash

# launch rpg_svo
roslaunch svo_ros live.launch cam_topic:=/camera/image_raw
# parameters
# rosparam load ../configs/svo/camera_atan.yaml
# rosparam load ../configs/svo/vo_fast.yaml


# run rqt_svo
# rosrun rqt_svo rqt_svo


# remote keys
# start: s
# quit: q
# reset: r
# rostopic pub /svo/remote_key std_msgs/String "s" -1
