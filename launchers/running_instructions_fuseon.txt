## Source each terminal
cd $FUSEON_STACK
source setup.sh localhost


## Roscore
roscore
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_roscore.sh


## Rosbag
$FUSEON_STACK/launchers/fuseon/rosbag_record.sh
$FUSEON_STACK/launchers/fuseon/rosbag_record_laptop.sh
$FUSEON_STACK/launchers/fuseon/rosbag_record_odroid01.sh
$FUSEON_STACK/launchers/fuseon/rosbag_record_odroid02.sh
$FUSEON_STACK/launchers/fuseon/rosbag_record_odroid03.sh


## Launch sensor drivers

# US Global Sat GPS
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_usglobalsat_gps.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_usglobalsat_gps.sh

# Phidgets GPS 1040
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_phidgets_gps_sync_1040_286007.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_phidgets_gps_sync_1040_286007.sh

# Phidgets IMU 1042
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_phidgets_imu_1042_297692.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_phidgets_imu_1042_297692.sh

# Phidgets IMU 1044
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_phidgets_imu_1044_354790.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_phidgets_imu_1044_354790.sh

# Asus Xtion Pro Live
# with openni
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_asus_xtion_pro_live.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_asus_xtion_pro_live.sh

# Hokuyo lidar
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_hokuyo_lidar_urg.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_hokuyo_lidar_urg.sh

# ueye Camera Left (Master)
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_ueye_cam_left.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_ueye_cam_left.sh

# ueye Camera Rigth (Slave)
roslaunch $FUSEON_STACK/launchers/sensors/fuseon_board_ueye_cam_right.launch
$FUSEON_STACK/launchers/sensors/screen/fuseon_board_ueye_cam_right.sh


## RViz
rosrun rviz rviz -d $FUSEON_STACK/configs/rviz/config_rviz.rviz




### Laptop
# Asus Xtion Pro Live

### fuseon_odroid_01
# Hokuyo lidar
# Phidgets IMU 1044
# Phidgets IMU 1042

### fuseon_odroid_02
# Phidgets GPS 1040
# ueye Camera Rigth (Slave)

### fuseon_odroid_03
# US Global Sat GPS
# ueye Camera Left (Master)




############# CACA!
### fuseon_odroid_01
# Asus Xtion Pro Live

### fuseon_odroid_02
# Phidgets IMU 1044
# Phidgets GPS 1040
# US Global Sat GPS
# ueye Camera Rigth (Slave)

### fuseon_odroid_03
# Phidgets IMU 1042
# Hokuyo lidar
# ueye Camera Left (Master)

