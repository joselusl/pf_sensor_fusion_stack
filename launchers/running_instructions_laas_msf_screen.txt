### Source each terminal
cd $FUSEON_STACK && source setup.sh maemi-wifi


### Create screen session
screen -AmdS laas_msf -t terminal bash


##################################
### ROSCORE
##################################
screen -S "laas_msf" -X screen -t "roscore" && \
screen -S "laas_msf" -p "roscore" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roscore\n'


##################################
### SENSORS
##################################

# imu phidgets 1044
screen -S "laas_msf" -X screen -t "phidgets_imu_1044" && \
screen -S "laas_msf" -p "phidgets_imu_1044" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/sensors/laas_phidgets_imu_1044_354790.launch\n'


# ueye camera
screen -S "laas_msf" -X screen -t "ueye_cam" && \
screen -S "laas_msf" -p "ueye_cam" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/sensors/laas_ueye_cam.launch\n'


# px4flow
screen -S "laas_msf" -X screen -t "px4flow" && \
screen -S "laas_msf" -p "px4flow" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/sensors/laas_px4flow.launch\n'


# Econ Camera - USB3
screen -S "laas_msf" -X screen -t "econCameraUVS" && \
screen -S "laas_msf" -p "econCameraUVS" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/sensors/laas_econCameraUVS_yuyv_usb3.launch\n'
# Econ Camera - USB2
screen -S "laas_msf" -X screen -t "econCameraUVS" && \
screen -S "laas_msf" -p "econCameraUVS" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/sensors/laas_econCameraUVS_yuyv_usb2.launch\n'


# asus xtion pro - openni
# roslaunch $FUSEON_STACK/launchers/sensors/laas_asus_xtion_pro_live_openni.launch
# asus xtion pro - openni2
screen -S "laas_msf" -X screen -t "asus_xtion" && \
screen -S "laas_msf" -p "asus_xtion" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/sensors/laas_asus_xtion_pro_live_openni2.launch\n'




##################################
### LAAS MOCAP
##################################

# Optitrack
screen -S "laas_msf" -X screen -t "optitrack" && \
screen -S "laas_msf" -p "optitrack" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && $FUSEON_STACK/packages/mocap/laas_optitrack/script/optitrack.sh\n'




##################################
### ARUCO_EYE
##################################

# Aruco Eye on ueye camera
screen -S "laas_msf" -X screen -t "aruco_eye_ros" && \
screen -S "laas_msf" -p "aruco_eye_ros" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/aruco_eye_ros/aruco_eye_ros_ueye_camera.launch\n'


# Rviz
rosrun rviz rviz -d $FUSEON_STACK/configs/rviz/config_rviz_aruco_eye.rviz



##################################
### Ground Speed -> px4flow
##################################

# Plot data
rqt_plot /fuseon/px4flow/px4flow/opt_flow/velocity_x /fuseon/px4flow/px4flow/opt_flow/velocity_y
rqt_plot /fuseon/px4flow/px4flow/opt_flow/ground_distance



##################################
### SVO
##################################

## Launch SVO
screen -S "laas_msf" -X screen -t "rpg_svo" && \
screen -S "laas_msf" -p "rpg_svo" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && roslaunch $FUSEON_STACK/launchers/mono_slam/rpg_svo/rpg_svo_ueyecam.launch\n'


## run rqt_svo
rosrun rqt_svo rqt_svo
# SVO namespace: /fuseon/ueye_camera/svo
# remote keys
# start: s
# quit: q
# reset: r
# rostopic pub /svo/remote_key std_msgs/String "s" -1

## Launch RVIZ
rosrun rviz rviz -d $FUSEON_STACK/configs/rviz/svo/config_rviz.rviz



##################################
### RGB-D SEGMENTATOR
##################################

# Segmentator
rosrun point_cloud_segmentation point_cloud_segmentation

# Rviz
rosrun rviz rviz -d $FUSEON_STACK/configs/rviz/config_asus_xtion.rviz



################################
### ROSBAG
################################

# Data 01
screen -S "laas_msf" -X screen -t "rosbag" && \
screen -S "laas_msf" -p "rosbag" -X stuff $'cd $FUSEON_STACK && source setup.sh maemi-wifi && $FUSEON_STACK/launchers/laas/rosbag_record_odroid_data01.sh\n'



