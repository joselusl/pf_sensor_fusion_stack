#!/bin/bash


# Change global defaults on /etc/openni/GlobalDefaults.ini


# launch asus xtion pro
roslaunch $FUSEON_STACK/launchers/sensors/asus_xtion_pro_live_2.launch \
                  rgb_camera_info_url:="$FUSEON_STACK/configs/sensors/asus_xtion_pro_live/rgb_camera.yaml" \
                  depth_camera_info_url:="$FUSEON_STACK/configs/sensors/asus_xtion_pro_live/depth_camera.yaml" \
                  depth_registration:=true \
                  publish_tf:=true
