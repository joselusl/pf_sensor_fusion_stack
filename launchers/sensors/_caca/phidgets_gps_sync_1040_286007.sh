#!/bin/bash

# launch phidgets_gps
roslaunch phidgets_gps phidgets_gps_sync.launch device_namespace:="phidgets_gps_1040" \
                                            node_name:="phidgets_gps_1040" \
                                            frame_id:="phidgets_gps_1040" \
                                            serial_number:=286007
