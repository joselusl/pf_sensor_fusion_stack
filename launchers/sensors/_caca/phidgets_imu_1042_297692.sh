#!/bin/bash

# launch phidgets_imu
roslaunch phidgets_imu phidgets_imu.launch device_namespace:="phidgets_imu_1042" \
                                            node_name:="phidgets_imu_1042" \
                                            frame_id:="phidgets_imu_1042" \
                                           serial_number:=297692
