#!/bin/bash

# launch phidgets_imu
roslaunch phidgets_imu phidgets_imu.launch device_namespace:="phidgets_imu_1044" \
                                            node_name:="phidgets_imu_1044" \
                                            frame_id:="phidgets_imu_1044" \
                                           serial_number:=354790
