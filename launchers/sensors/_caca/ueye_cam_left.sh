#!/bin/bash

# launch ueye_cam left -> master
roslaunch $FUSEON_STACK/launchers/sensors/ueye_cam.launch device_namespace:="ueye_camera_left" \
                  camera_topic:="image_raw" \
                  camera_id:="1" \
                  camera_intrinsics_file:="$FUSEON_STACK/configs/sensors/ueye_camera_left/camera_calib.yaml" \
                  camera_parameters_file:="$FUSEON_STACK/configs/sensors/ueye_camera_left/UI122xLE-C_conf.ini" \
                  color_mode:="rgb8" \
                  ext_trigger_mode:="False"

