#!/bin/bash

# run usglobalsat_gps
roslaunch $FUSEON_STACK/launchers/sensors/nmea_serial_driver.launch device_namespace:="usglobalsat_gps" \
            node_name:="usglobalsat_gps" \
            frame_id:="usglobalsat_gps" \
            port:="/dev/ttyUSB0"

