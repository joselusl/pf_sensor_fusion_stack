% Pose is a struct
function p3=pose_add(p1, p2)

    % Path
    if(ispc)
      addpath('Z:\Work\workspace\Matlab\quat')
    elseif(isunix)
      addpath('/media/truecrypt10/Work/workspace/Matlab/quat')
    end


    % check dimensions
    % p1
    if(size(p1.position,2) ~= size(p1.attitude,2))
        error('size(p1.position,2) ~= size(p1.attitude,2)')
    end
    if(size(p1.position,1) ~= 3)
        error('size(p1.position,1) ~= 3')
    end
    if(size(p1.attitude,1) ~= 4)
        error('size(p1.attitude,1) ~= 4')
    end
    % p2
    if(size(p2.position,2) ~= size(p2.attitude,2))
        error('size(p12.position,2) ~= size(p2.attitude,2)')
    end
    if(size(p2.position,1) ~= 3)
        error('size(p2.position,1) ~= 3')
    end
    if(size(p2.attitude,1) ~= 4)
        error('size(p2.attitude,1) ~= 4')
    end
    
    % check data dimensions
    dim_p1=size(p1.position,2);
    dim_p2=size(p2.position,2);  
    
    if(dim_p1 == 1 && dim_p2 == 1)
        dim = 1;
        sit = 1;
    elseif(dim_p1 == 1 && dim_p2 ~= 1)
        dim = dim_p2;
        sit = 2;
    elseif(dim_p1 ~= 1 && dim_p2 == 1)
        dim = dim_p1;
        sit = 3;
    else
        if(dim_p1 ~= dim_p2)
            error('dim_p1 ~= dim_p2');
        else
            dim = dim_p1;
            sit = 4;
        end
    end
    

    %%% p3
    
    % reserve
    p3.position=zeros(3, dim);
    p3.attitude=zeros(4, dim);

    
    % t3
    t3a=quat_cross(p1.attitude, [zeros(1, size(p2.position,2)); p2.position],quat_inv(p1.attitude, true));
    
    switch(sit)
        case 1
            p3.position(:,1)=t3a(2:4,1)+p1.position(:,1);
        case 2
            for(i=1:dim) 
                p3.position(:,i)=t3a(2:4,i)+p1.position(:,1);
            end
        case 3
            for(i=1:dim) 
                p3.position(:,i)=t3a(2:4,1)+p1.position(:,i);
            end
        case 4
            p3.position=t3a(2:4,:)+p1.position;
    end


    % q3
    p3.attitude=quat_cross(p1.attitude, p2.attitude);
    




end


