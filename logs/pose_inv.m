% Pose is a struct
function p2=pose_inv(p1)
    
    % Path
    if(ispc)
      addpath('Z:\Work\workspace\Matlab\quat')
    elseif(isunix)
      addpath('/media/truecrypt10/Work/workspace/Matlab/quat')
    end


    p2ta=-quat_cross(quat_cross(quat_inv(p1.attitude, true),[0; p1.position]),p1.attitude);
    p2.position=p2ta(2:4,:);
    p2.attitude=quat_inv(p1.attitude, true);


end
