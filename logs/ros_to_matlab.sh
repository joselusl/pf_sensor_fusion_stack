#! /bin/bash

# bag name
#BAG_FILE='session5_2016-06-14-17-32-43'
#BAG_FILE='session5_2016-06-14-19-02-54'
#BAG_FILE='session5_2016-06-14-19-09-00'
#BAG_FILE='session6_2016-06-15-16-19-24'
#BAG_FILE='session6_2016-06-15-18-08-16'
#BAG_FILE='session7_2016-06-28-14-00-54'
#BAG_FILE='session7_2016-06-28-14-18-10'
#BAG_FILE='session9_2016-08-10-13-43-04'
#BAG_FILE='session9_2016-08-10-14-06-59'
#BAG_FILE='session9_2016-08-10-14-18-18'
#BAG_FILE='session9_2016-08-10-14-21-40'
#BAG_FILE='session9_2016-08-10-14-26-08'
#BAG_FILE='session9_2016-08-10-15-00-10'
#BAG_FILE='session9_2016-08-10-17-06-48'
#BAG_FILE='session9_2016-08-10-18-16-10'
#BAG_FILE='session9_2016-08-17-20-44-09' # In doulou, first flight
#BAG_FILE='session9_2016-08-17-20-56-44' # In doulou, second flight

#BAG_FILE='session10_2016-08-26-15-05-17' # new session with new qr config
#BAG_FILE='session10_2016-08-26-18-09-44' # primer vuelo no mocap guardado

#BAG_FILE='session12_2016-09-08-14-42-44' # With Matlab data
#BAG_FILE='session12_2016-09-08-14-45-56' # With Matlab data
#BAG_FILE='session12_2016-09-08-14-49-51' # With Matlab data
#BAG_FILE='session12_2016-09-08-14-52-34' 
#BAG_FILE='session12_2016-09-08-14-56-07' # With Matlab data
#BAG_FILE='session12_2016-09-08-15-01-45'
#BAG_FILE='session12_2016-09-08-15-38-07' # With Matlab data
#BAG_FILE='session12_2016-09-08-15-41-00'

BAG_FILE='session1_2016-10-24-17-17-59' # Victor: Sine with Matlab
#BAG_FILE='session1_2016-10-24-19-04-39' # Victor: Circular with Matlab




# MOCAP POSE
echo "+MoCap pose"
#rostopic echo /tkbridge/MSF -b $BAG_FILE'.bag' -p >  mocap_pose.csv
rostopic echo /tkbridge/QR_4 -b $BAG_FILE'.bag' -p >  mocap_pose.csv
echo " MoCap pose done"


# IMU Measurements
echo "+IMU measurements"
rostopic echo /fuseon/phidgets_imu_1044/imu/data_raw -b $BAG_FILE'.bag' -p >  imu_meas.csv
echo " IMU measurements done"


# ARUCO_SLAM ROBOT Estimated
#rostopic echo /fuseon/msf_localization_aruco_slam/robot_pose_cov -b $BAG_FILE'.bag' -p >  aruco_slam_robot_pose.csv


# ROBOT Estimated
# ROBOT Pose
echo "+Robot Pose Cov"
rostopic echo /fuseon/msf_localization/robot_pose_cov -b $BAG_FILE'.bag' -p >  robot_pose.csv
echo " Robot Pose Cov done"
# ROBOT Velocity
echo "+Robot Velocity Cov"
rostopic echo /fuseon/msf_localization/robot_velocity_cov -b $BAG_FILE'.bag' -p >  robot_velocity.csv
echo " Robot Velocity Cov done"
# ROBOT Acceleration
echo "+Robot Acceleration Cov"
rostopic echo /fuseon/msf_localization/robot_acceleration_cov -b $BAG_FILE'.bag' -p >  robot_acceleration.csv
echo " Robot Acceleration Cov done"

# IMU Estimated
echo "+IMU estimated bias lin acc"
rostopic echo /fuseon/phidgets_imu_1044/imu/estimated_biases_linear_acceleration -b $BAG_FILE'.bag' -p >  imu_estimated_bias_lin_acc.csv
echo " IMU estimated bias lin acc done"

# TF
echo "+TF"
rostopic echo /tf -b $BAG_FILE'.bag' -p >  tf.csv
echo " TF done"


