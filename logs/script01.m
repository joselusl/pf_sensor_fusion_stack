%%% MSF: IMU + MOCAP

close all;



%% IMU Meas lin accel

figure
subplot(3,1,1)
plot(imu_meas.time, imu_meas.linear_acc(1,:),'.r')
title('imu ax')
grid on

subplot(3,1,2)
plot(imu_meas.time, imu_meas.linear_acc(2,:),'.r')
title('imu ay')
grid on

subplot(3,1,3)
plot(imu_meas.time, imu_meas.linear_acc(3,:),'.r')
title('imu az')
grid on



%% IMU Meas ang vel

figure
subplot(3,1,1)
plot(imu_meas.time, imu_meas.angular_vel(1,:),'.r')
title('imu \omegax')
grid on

subplot(3,1,2)
plot(imu_meas.time, imu_meas.angular_vel(2,:),'.r')
title('imu \omegay')
grid on

subplot(3,1,3)
plot(imu_meas.time, imu_meas.angular_vel(3,:),'.r')
title('imu \omegaz')
grid on




%% Pos
figure

subplot(3,1,1)
plot(estim_pose_robot_wrt_world.time, estim_pose_robot_wrt_world.position(1,:)','.b')
hold on;
plot(meas_pose_mocap_wrt_mocap_world.time,meas_pose_mocap_wrt_mocap_world.position(1,:)','.r')
title('x')
grid on



% figure
subplot(3,1,2)
plot(estim_pose_robot_wrt_world.time, estim_pose_robot_wrt_world.position(2,:)','.b')
hold on;
plot(meas_pose_mocap_wrt_mocap_world.time,meas_pose_mocap_wrt_mocap_world.position(2,:)','.r')
title('y')
grid on


% figure
subplot(3,1,3)
plot(estim_pose_robot_wrt_world.time, estim_pose_robot_wrt_world.position(3,:)','.b')
hold on;
plot(meas_pose_mocap_wrt_mocap_world.time,meas_pose_mocap_wrt_mocap_world.position(3,:)','.r')
title('z')
grid on



%% Lin Vel
figure
subplot(3,1,1)
plot(estim_vel_robot_wrt_world.time, estim_vel_robot_wrt_world.linear(1,:)','.b')
hold on;
title('vx')
grid on

% figure
subplot(3,1,2)
plot(estim_vel_robot_wrt_world.time, estim_vel_robot_wrt_world.linear(2,:)','.b')
hold on;
title('vy')
grid on

% figure
subplot(3,1,3)
plot(estim_vel_robot_wrt_world.time, estim_vel_robot_wrt_world.linear(3,:)','.b')
hold on;
title('vz')
grid on




%% Lin Acc
figure
subplot(3,1,1)
plot(estim_acc_robot_wrt_world.time, estim_acc_robot_wrt_world.linear(1,:)','.b')
hold on;
title('ax')
grid on

% figure
subplot(3,1,2)
plot(estim_acc_robot_wrt_world.time, estim_acc_robot_wrt_world.linear(2,:)','.b')
hold on;
title('ay')
grid on

% figure
subplot(3,1,3)
plot(estim_acc_robot_wrt_world.time, estim_acc_robot_wrt_world.linear(3,:)','.b')
hold on;
title('az')
grid on



%% Attitude: quaternion

figure
subplot(2,2,1)
plot(estim_pose_robot_wrt_world.time, estim_pose_robot_wrt_world.attitude(1,:)','.b')
hold on;
plot(meas_pose_mocap_wrt_mocap_world.time,meas_pose_mocap_wrt_mocap_world.attitude(1,:)','.r')
title('qw')
grid on

subplot(2,2,2)
plot(estim_pose_robot_wrt_world.time, estim_pose_robot_wrt_world.attitude(2,:)','.b')
hold on;
plot(meas_pose_mocap_wrt_mocap_world.time,meas_pose_mocap_wrt_mocap_world.attitude(2,:)','.r')
title('qx')
grid on

subplot(2,2,3)
plot(estim_pose_robot_wrt_world.time, estim_pose_robot_wrt_world.attitude(3,:)','.b')
hold on;
plot(meas_pose_mocap_wrt_mocap_world.time,meas_pose_mocap_wrt_mocap_world.attitude(3,:)','.r')
title('qy')
grid on

subplot(2,2,4)
plot(estim_pose_robot_wrt_world.time, estim_pose_robot_wrt_world.attitude(4,:)','.b')
hold on;
plot(meas_pose_mocap_wrt_mocap_world.time,meas_pose_mocap_wrt_mocap_world.attitude(4,:)','.r')
title('qz')
grid on



%% Attitude angles
if(0)
    
    figure
    subplot(3,1,1)
    plot(estim_pose_robot_wrt_world.time, 180/pi*estim_pose_robot_wrt_world.attitude_angles(1,:)','.b')
    hold on;
    plot(meas_pose_mocap_wrt_mocap_world.time, 180/pi*meas_pose_mocap_wrt_mocap_world.attitude_angles(1,:)','.r')
    title('angle 1')
    grid on



    % figure
    subplot(3,1,2)
    plot(estim_pose_robot_wrt_world.time, 180/pi*estim_pose_robot_wrt_world.attitude_angles(2,:)','.b')
    hold on;
    plot(meas_pose_mocap_wrt_mocap_world.time, 180/pi*meas_pose_mocap_wrt_mocap_world.attitude_angles(2,:)','.r')
    title('angle 2')
    grid on


    % figure
    subplot(3,1,3)
    plot(estim_pose_robot_wrt_world.time, 180/pi*estim_pose_robot_wrt_world.attitude_angles(3,:)','.b')
    hold on;
    plot(meas_pose_mocap_wrt_mocap_world.time, 180/pi*meas_pose_mocap_wrt_mocap_world.attitude_angles(3,:)','.r')
    title('angle 3')
    grid on

end



%% Angular Velocity
figure
subplot(3,1,1)
plot(estim_vel_robot_wrt_world.time, estim_vel_robot_wrt_world.angular(1,:)','.b')
hold on;
title('\omegax')
grid on

% figure
subplot(3,1,2)
plot(estim_vel_robot_wrt_world.time, estim_vel_robot_wrt_world.angular(2,:)','.b')
hold on;
title('\omegay')
grid on

% figure
subplot(3,1,3)
plot(estim_vel_robot_wrt_world.time, estim_vel_robot_wrt_world.angular(3,:)','.b')
hold on;
title('\omegaz')
grid on



%% Angular Acceleration
figure
subplot(3,1,1)
plot(estim_acc_robot_wrt_world.time, estim_acc_robot_wrt_world.angular(1,:)','.b')
hold on;
title('\alphax')
grid on

% figure
subplot(3,1,2)
plot(estim_acc_robot_wrt_world.time, estim_acc_robot_wrt_world.angular(2,:)','.b')
hold on;
title('\alphay')
grid on

% figure
subplot(3,1,3)
plot(estim_acc_robot_wrt_world.time, estim_acc_robot_wrt_world.angular(3,:)','.b')
hold on;
title('\alphaz')
grid on



%% Estim bias lin acc

figure
subplot(3,1,1)
plot(imu_estimated_bias_lin_acc.time, imu_estimated_bias_lin_acc.value(1,:)','.b')
hold on;
title('bax')
grid on

% figure
subplot(3,1,2)
plot(imu_estimated_bias_lin_acc.time, imu_estimated_bias_lin_acc.value(2,:)','.b')
hold on;
title('bay')
grid on

% figure
subplot(3,1,3)
plot(imu_estimated_bias_lin_acc.time, imu_estimated_bias_lin_acc.value(3,:)','.b')
hold on;
title('baz')
grid on




%% 3D traj
figure
plot3( estim_pose_robot_wrt_world.position(1,:)',  estim_pose_robot_wrt_world.position(2,:)',  estim_pose_robot_wrt_world.position(3,:)','.b')
hold on
plot3(meas_pose_mocap_wrt_mocap_world.position(1,:)',meas_pose_mocap_wrt_mocap_world.position(2,:)',meas_pose_mocap_wrt_mocap_world.position(3,:)','.r')
grid on
xlabel('x')
ylabel('y')
zlabel('z')



