% Start
%clear all
close all
clc





%% MOCAP POSE

disp('+processing mocap measurements')
clear meas_pose_mocap_wrt_mocap_world
meas_pose_mocap_wrt_mocap_world=importFilePoseStamped('mocap_pose.csv');


%% IMU MEASUREMENTS

disp('+processing imu measurements')
clear imu_meas
imu_meas=importFileImu('imu_meas.csv');


%% ARUCO SLAM ROBOT POSE

% disp('+processing estimation aruco slam robot pose')
% clear estim_aruco_slam_pose_robot_wrt_world
% estim_aruco_slam_pose_robot_wrt_world=importFilePoseWithCovarianceStamped('aruco_slam_robot_pose.csv');


%% ROBOT POSE

disp('+processing estimation robot pose')
clear estim_pose_robot_wrt_world
estim_pose_robot_wrt_world=importFilePoseWithCovarianceStamped('robot_pose.csv');


%% Robot Velocity

disp('+processing estimation robot velocity')
clear estim_vel_robot_wrt_world
estim_vel_robot_wrt_world=importFileTwistWithCovarianceStamped('robot_velocity.csv');


%% Robot Acceleration

disp('+processing estimation robot acceleration')
clear estim_acc_robot_wrt_world
estim_acc_robot_wrt_world=importFileAccelWithCovarianceStamped('robot_acceleration.csv');


%% IMU ESTIMATED BIAS

disp('+processing estimation imu bias lin acc')
clear imu_estimated_bias_lin_acc
imu_estimated_bias_lin_acc=importFileVectorStamped('imu_estimated_bias_lin_acc.csv');


%% TF

disp('+processing tf')
clear tf
tf=importFileTf('tf.csv');



%% FREQUENCY ON THE ESTIMATION

freq=length(estim_pose_robot_wrt_world.time)/(estim_pose_robot_wrt_world.time(length(estim_pose_robot_wrt_world.time))-estim_pose_robot_wrt_world.time(1));
disp(['-> Average frequency on estimation = ',num2str(freq),' Hz'])



%%

return;


%% FIX THE TIME STAMPS

time_ref=estim_pose_robot_wrt_world.time(1);


% Mocap pose
meas_pose_mocap_wrt_mocap_world.time=meas_pose_mocap_wrt_mocap_world.time-time_ref;

% Imu
imu_meas.time=imu_meas.time-time_ref;


% estim robot pose
estim_pose_robot_wrt_world.time=estim_pose_robot_wrt_world.time-time_ref;

% estim robot velocity
estim_vel_robot_wrt_world.time=estim_vel_robot_wrt_world.time-time_ref;

% estim robot acc
estim_acc_robot_wrt_world.time=estim_acc_robot_wrt_world.time-time_ref;

% imu estimated bias lin acc
imu_estimated_bias_lin_acc.time=imu_estimated_bias_lin_acc.time-time_ref;


%% FIX ATTITUDES (q=-q)

% mocap
if(meas_pose_mocap_wrt_mocap_world.attitude(1,:)<0)
    meas_pose_mocap_wrt_mocap_world.attitude = -meas_pose_mocap_wrt_mocap_world.attitude;
end




%% Convert angles
return

% mocap pose
[meas_pose_mocap_wrt_mocap_world.attitude_angles(1,:), meas_pose_mocap_wrt_mocap_world.attitude_angles(2,:), meas_pose_mocap_wrt_mocap_world.attitude_angles(3,:)]=quat2angle(meas_pose_mocap_wrt_mocap_world.attitude');



% estim robot pose
[estim_pose_robot_wrt_world.attitude_angles(1,:), estim_pose_robot_wrt_world.attitude_angles(2,:), estim_pose_robot_wrt_world.attitude_angles(3,:)]=quat2angle(estim_pose_robot_wrt_world.attitude');






return

%% ADJUST DATA with MOCAP

disp('+adjusting data with MoCap')
if( size(meas_pose_mocap_wrt_mocap_world.position,2) ~= 0 || size(meas_pose_mocap_wrt_mocap_world.attitude,2) ~= 0 )

    % pose_mocap_wrt_robot
    pose_mocap_wrt_robot.position=[0.027; 0.0; 0.119];
    pose_mocap_wrt_robot.attitude=[1;0;0;0];

    % meas_pose_robot_wrt_mocap_world

    meas_pose_robot_wrt_mocap_world=pose_add(meas_pose_mocap_wrt_mocap_world, pose_inv(pose_mocap_wrt_robot));
    meas_pose_robot_wrt_mocap_world.time=meas_pose_mocap_wrt_mocap_world.time;




    % pose_mocap_world_wrt_world

    figure
    plot(estim_pose_robot_wrt_world.position(1,:))

    data_estim_conv=500;
    time_estim_conv=estim_pose_robot_wrt_world.time(500);

    data_meas_conv=find(meas_pose_robot_wrt_mocap_world.time<time_estim_conv,1, 'last');

    % TODO

    p1.position=estim_pose_robot_wrt_world.position(:,data_estim_conv);
    p1.attitude=estim_pose_robot_wrt_world.attitude(:,data_estim_conv);

    p2.position=meas_pose_robot_wrt_mocap_world.position(:,data_meas_conv);
    p2.attitude=meas_pose_robot_wrt_mocap_world.attitude(:,data_meas_conv);

    pose_mocap_world_wrt_world=pose_add(p1, pose_inv(p2));
    %pose_mocap_world_wrt_world.time=


    % meas_pose_robot_wrt_world

    meas_pose_robot_wrt_world=pose_add(pose_mocap_world_wrt_world, meas_pose_robot_wrt_mocap_world);
    meas_pose_robot_wrt_world.time=meas_pose_robot_wrt_mocap_world.time;

else
    
    % Set mocap variables to zero
    meas_pose_robot_wrt_world.time=zeros(1,0);
    meas_pose_robot_wrt_world.position=zeros(3,0);
    meas_pose_robot_wrt_world.attitude=zeros(4,0);
    
end
    


%% ADJUST TIME STAMP

disp('+adjusting time stamp')

% Time
time_inc=estim_pose_robot_wrt_world.time(1);


% Mocap
mocap_fixed.time=meas_pose_robot_wrt_world.time-time_inc;
mocap_fixed.position=meas_pose_robot_wrt_world.position;
mocap_fixed.attitude=meas_pose_robot_wrt_world.attitude;

% Imu measurement
imu_meas_fixed.time=imu_meas.time-time_inc;
imu_meas_fixed.linear_acc=imu_meas.linear_acc;
imu_meas_fixed.angular_vel=imu_meas.angular_vel;


% Estim Aruco SLAM Robot Pose
aruco_slam_robot_fixed.time=estim_aruco_slam_pose_robot_wrt_world.time-time_inc;
aruco_slam_robot_fixed.position=estim_aruco_slam_pose_robot_wrt_world.position;
aruco_slam_robot_fixed.attitude=estim_aruco_slam_pose_robot_wrt_world.attitude;

% Estim Robot Pose
robot_fixed.time=estim_pose_robot_wrt_world.time-time_inc;
robot_fixed.position=estim_pose_robot_wrt_world.position;
robot_fixed.attitude=estim_pose_robot_wrt_world.attitude;

% Estim Robot Velo
estim_vel_robot_wrt_world_fixed.time=estim_vel_robot_wrt_world.time-time_inc;
estim_vel_robot_wrt_world_fixed.linear=estim_vel_robot_wrt_world.linear;
estim_vel_robot_wrt_world_fixed.angular=estim_vel_robot_wrt_world.angular;

% Estim Robot Acce
estim_acc_robot_wrt_world_fixed.time=estim_acc_robot_wrt_world.time-time_inc;
estim_acc_robot_wrt_world_fixed.linear=estim_acc_robot_wrt_world.linear;
estim_acc_robot_wrt_world_fixed.angular=estim_acc_robot_wrt_world.angular;


% Estim IMU bias lin acc
imu_estimated_bias_lin_acc_fixed.time=imu_estimated_bias_lin_acc.time-time_inc;
imu_estimated_bias_lin_acc_fixed.value=imu_estimated_bias_lin_acc.value;




%% PLOT
close all

disp('+plotting')

%%% IMU Meas

% Imu Meas Acc Line
figure
subplot(3,1,1)
plot(imu_meas_fixed.time, imu_meas_fixed.linear_acc(1,:),'.-')
grid on
title('imu meas linear acc ax')
xlabel('time (s)')

subplot(3,1,2)
plot(imu_meas_fixed.time, imu_meas_fixed.linear_acc(2,:),'.-')
grid on
title('imu meas linear acc ay')
xlabel('time (s)')

subplot(3,1,3)
plot(imu_meas_fixed.time, imu_meas_fixed.linear_acc(3,:),'.-')
grid on
title('imu meas linear acc az')
xlabel('time (s)')


% Imu Meas Ang Acc
figure
subplot(3,1,1)
plot(imu_meas_fixed.time, imu_meas_fixed.angular_vel(1,:),'.-')
grid on
title('imu meas angular vel wx')
xlabel('time (s)')

subplot(3,1,2)
plot(imu_meas_fixed.time, imu_meas_fixed.angular_vel(2,:),'.-')
grid on
title('imu meas angular vel wy')
xlabel('time (s)')

subplot(3,1,3)
plot(imu_meas_fixed.time, imu_meas_fixed.angular_vel(3,:),'.-')
grid on
title('imu meas angular vel wz')
xlabel('time (s)')


%%% 
figure
subplot(3,1,1)
plot(robot_fixed.time,robot_fixed.position(1,:), '.-')
hold on;
plot(aruco_slam_robot_fixed.time,aruco_slam_robot_fixed.position(1,:), 'g.-')
plot(mocap_fixed.time,mocap_fixed.position(1,:),'r.-')
grid on
legend('robot','aruco slam','mocap')
title('x')
xlabel('time (s)')

% figure
subplot(3,1,2)
plot(robot_fixed.time,robot_fixed.position(2,:), '.-')
hold on;
plot(aruco_slam_robot_fixed.time,aruco_slam_robot_fixed.position(2,:), 'g.-')
plot(mocap_fixed.time,mocap_fixed.position(2,:),'r.-')
grid on
legend('robot','aruco slam','mocap')
title('y')
xlabel('time (s)')

% figure
subplot(3,1,3)
plot(robot_fixed.time,robot_fixed.position(3,:), '.-')
hold on;
plot(aruco_slam_robot_fixed.time,aruco_slam_robot_fixed.position(3,:), 'g.-')
plot(mocap_fixed.time,mocap_fixed.position(3,:),'r.-')
grid on
legend('robot','aruco slam','mocap')
title('z')
xlabel('time (s)')


%%%%
figure
subplot(2,2,1)
plot(robot_fixed.time,robot_fixed.attitude(1,:), '.-')
hold on;
plot(mocap_fixed.time,mocap_fixed.attitude(1,:),'r.-')
grid on
legend('robot','mocap')
title('qw')
xlabel('time (s)')

% figure
subplot(2,2,2)
plot(robot_fixed.time,robot_fixed.attitude(2,:), '.-')
hold on;
plot(mocap_fixed.time,mocap_fixed.attitude(2,:),'r.-')
grid on
legend('robot','mocap')
title('qx')
xlabel('time (s)')

% figure
subplot(2,2,3)
plot(robot_fixed.time,robot_fixed.attitude(3,:), '.-')
hold on;
plot(mocap_fixed.time,mocap_fixed.attitude(3,:),'r.-')
grid on
legend('robot','mocap')
title('qy')
xlabel('time (s)')

% figure
subplot(2,2,4)
plot(robot_fixed.time,robot_fixed.attitude(4,:), '.-')
hold on;
plot(mocap_fixed.time,mocap_fixed.attitude(4,:),'r.-')
grid on
legend('robot','mocap')
title('qz')
xlabel('time (s)')


% Robot Estim Vel Line
figure
subplot(3,1,1)
plot(estim_vel_robot_wrt_world_fixed.time, estim_vel_robot_wrt_world_fixed.linear(1,:),'.-')
grid on
title('robot estimated linear velocity vx')
xlabel('time (s)')

subplot(3,1,2)
plot(estim_vel_robot_wrt_world_fixed.time, estim_vel_robot_wrt_world_fixed.linear(2,:),'.-')
grid on
title('robot estimated linear velocity vy')
xlabel('time (s)')

subplot(3,1,3)
plot(estim_vel_robot_wrt_world_fixed.time, estim_vel_robot_wrt_world_fixed.linear(3,:),'.-')
grid on
title('robot estimated linear velocity vz')
xlabel('time (s)')


% Robot Estim Vel Angular
figure
subplot(3,1,1)
plot(estim_vel_robot_wrt_world_fixed.time, estim_vel_robot_wrt_world_fixed.angular(1,:),'.-')
grid on
title('robot estimated angular velocity wx')
xlabel('time (s)')

subplot(3,1,2)
plot(estim_vel_robot_wrt_world_fixed.time, estim_vel_robot_wrt_world_fixed.angular(2,:),'.-')
grid on
title('robot estimated angular velocity wy')
xlabel('time (s)')

subplot(3,1,3)
plot(estim_vel_robot_wrt_world_fixed.time, estim_vel_robot_wrt_world_fixed.angular(3,:),'.-')
grid on
title('robot estimated angular velocity wz')
xlabel('time (s)')


% Robot Estim Acc Line
figure
subplot(3,1,1)
plot(estim_acc_robot_wrt_world_fixed.time, estim_acc_robot_wrt_world_fixed.linear(1,:),'.-')
grid on
title('robot estimated linear acc ax')
xlabel('time (s)')

subplot(3,1,2)
plot(estim_acc_robot_wrt_world_fixed.time, estim_acc_robot_wrt_world_fixed.linear(2,:),'.-')
grid on
title('robot estimated linear acc ay')
xlabel('time (s)')

subplot(3,1,3)
plot(estim_acc_robot_wrt_world_fixed.time, estim_acc_robot_wrt_world_fixed.linear(3,:),'.-')
grid on
title('robot estimated linear acc az')
xlabel('time (s)')


% Robot Estim Acc Angular
figure
subplot(3,1,1)
plot(estim_acc_robot_wrt_world_fixed.time, estim_acc_robot_wrt_world_fixed.angular(1,:),'.-')
grid on
title('robot estimated angular acc alphax')
xlabel('time (s)')

subplot(3,1,2)
plot(estim_acc_robot_wrt_world_fixed.time, estim_acc_robot_wrt_world_fixed.angular(2,:),'.-')
grid on
title('robot estimated angular acc alphay')
xlabel('time (s)')

subplot(3,1,3)
plot(estim_acc_robot_wrt_world_fixed.time, estim_acc_robot_wrt_world_fixed.angular(3,:),'.-')
grid on
title('robot estimated angular acc alphaz')
xlabel('time (s)')




% IMU Estimated bias lin acc
figure
plot(imu_estimated_bias_lin_acc_fixed.time, imu_estimated_bias_lin_acc_fixed.value,'.-')
grid on
legend('bax','bay', 'baz')
title('imu estimated bias lin acc')
xlabel('time (s)')
