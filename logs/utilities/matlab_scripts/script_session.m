%% Clean
close all
clear all
clc


%% Common Vars

stack_path='/home/joselusl/workspace/ros/pf_sensor_fusion_catkin_ws/src/fuseon_stack/';



%% Import variables


%% Chose if import from CSV or from MATLAB data

flagImportCSV=1;
flagPostProcessData=1;
matlab_data_path=[stack_path,'logs/session1/matlab_data/session1_2015-08-05-16-24-49'];

%%
if(flagImportCSV)
    
    %% Import from CSV
    csv_path=[stack_path,'logs/session1/csv/session1_2015-08-05-16-24-49'];
    
    matlab_functions_path=[stack_path,'logs/utilities/matlab_functions/importfile'];
    addpath(matlab_functions_path);


    % phidgets_gps_1040
    disp('Importing phidgets_gps_1040')
    disp(' Importing fuseon_phidgets_gps_1040_gps_nav_sat_fix')
    fuseon_phidgets_gps_1040_gps_nav_sat_fix=importfile_sensor_msgs_NavSatFix([csv_path,'/fuseon_phidgets_gps_1040_gps_nav_sat_fix.csv']);
    disp(' Importing fuseon_phidgets_gps_1040_gps_nav_sat_status')
    fuseon_phidgets_gps_1040_gps_nav_sat_status=importfile_sensor_msgs_NavSatStatus([csv_path,'/fuseon_phidgets_gps_1040_gps_nav_sat_status.csv']);
    disp(' Importing fuseon_phidgets_gps_1040_time_reference')
    fuseon_phidgets_gps_1040_time_reference=importfile_sensor_msgs_TimeReference([csv_path,'/fuseon_phidgets_gps_1040_time_reference.csv']);

    % usglobalsat_gps
    disp('Importing usglobalsat_gps')
    disp(' Importing fuseon_usglobalsat_gps_nav_sat_fix')
    fuseon_usglobalsat_gps_nav_sat_fix=importfile_sensor_msgs_NavSatFix([csv_path,'/fuseon_usglobalsat_gps_nav_sat_fix.csv']);
    disp(' Importing fuseon_usglobalsat_gps_nav_sat_vel')
    fuseon_usglobalsat_gps_nav_sat_vel=importfile_geometry_msgs_TwistStamped([csv_path,'/fuseon_usglobalsat_gps_nav_sat_vel.csv']);
    disp(' Importing fuseon_usglobalsat_gps_time_reference')
    fuseon_usglobalsat_gps_time_reference=importfile_sensor_msgs_TimeReference([csv_path,'/fuseon_usglobalsat_gps_time_reference.csv']);

    % phidgets_imu_1042
    disp('Importing phidgets_imu_1042')
    disp(' Importing fuseon_phidgets_imu_1042_imu_data_raw')
    fuseon_phidgets_imu_1042_imu_data_raw=importfile_sensor_msgs_Imu([csv_path,'/fuseon_phidgets_imu_1042_imu_data_raw.csv']);
    disp(' Importing fuseon_phidgets_imu_1042_mag_data_raw')
    fuseon_phidgets_imu_1042_mag_data_raw=importfile_geometry_msgs_Vector3Stamped([csv_path,'/fuseon_phidgets_imu_1042_mag_data_raw.csv']);

    % phidgets_imu_1044
    disp('Importing phidgets_imu_1044')
    disp(' Importing fuseon_phidgets_imu_1044_imu_data_raw')
    fuseon_phidgets_imu_1044_imu_data_raw=importfile_sensor_msgs_Imu([csv_path,'/fuseon_phidgets_imu_1044_imu_data_raw.csv']);
    disp(' Importing fuseon_phidgets_imu_1044_mag_data_raw')
    fuseon_phidgets_imu_1044_mag_data_raw=importfile_geometry_msgs_Vector3Stamped([csv_path,'/fuseon_phidgets_imu_1044_mag_data_raw.csv']);

    
    %% Post process data 
    
    if(flagPostProcessData)
        % Clean nan and convert to double from string

        %fuseon_phidgets_imu_1042_mag_data_raw
        for(i=size(fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorx,1):-1:1)
            if(strcmp(fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorx{i,1},'nan'))
                fuseon_phidgets_imu_1042_mag_data_raw(i,:)=[];
            end
        end

        fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorx=str2double(fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorx);
        fuseon_phidgets_imu_1042_mag_data_raw.fieldvectory=str2double(fuseon_phidgets_imu_1042_mag_data_raw.fieldvectory);
        fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorz=str2double(fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorz);


        %fuseon_phidgets_imu_1044_mag_data_raw
        for(i=size(fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorx,1):-1:1)
            if(strcmp(fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorx{i,1},'nan'))
                fuseon_phidgets_imu_1044_mag_data_raw(i,:)=[];
            end
        end

        fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorx=str2double(fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorx);
        fuseon_phidgets_imu_1044_mag_data_raw.fieldvectory=str2double(fuseon_phidgets_imu_1044_mag_data_raw.fieldvectory);
        fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorz=str2double(fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorz);
    
    end
    
    
    %% Save workspace

    save(matlab_data_path);
    
else
    %% Import from MATLAB data
    load(matlab_data_path);
    
    
end



%% Plot
close all

% GPS
% phidgets_gps_1040
figure
plot3(fuseon_phidgets_gps_1040_gps_nav_sat_fix.fieldlatitude, fuseon_phidgets_gps_1040_gps_nav_sat_fix.fieldlongitude, fuseon_phidgets_gps_1040_gps_nav_sat_fix.fieldaltitude, 'r')
hold on;


% usglobalsat_gps
plot3(fuseon_usglobalsat_gps_nav_sat_fix.fieldlatitude, fuseon_usglobalsat_gps_nav_sat_fix.fieldlongitude, fuseon_usglobalsat_gps_nav_sat_fix.fieldaltitude, 'm')

title('GPS')
xlabel('latitude')
ylabel('longitude')
zlabel('altitude')
grid on;


% IMU - Magnetometers

time_fuseon_phidgets_imu_1042_mag_data_raw=(fuseon_phidgets_imu_1042_mag_data_raw.VarName1-fuseon_phidgets_imu_1042_mag_data_raw.VarName1(1))/1e9;
time_fuseon_phidgets_imu_1044_mag_data_raw=(fuseon_phidgets_imu_1044_mag_data_raw.VarName1-fuseon_phidgets_imu_1044_mag_data_raw.VarName1(1))/1e9;


% Angles
figure
%alpha_x
subplot(3,1,1)
plot(time_fuseon_phidgets_imu_1042_mag_data_raw,fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorx,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_mag_data_raw,fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorx,'r')
title('alpha_x')
xlabel('time (s)')
ylabel('angle (rad)')
grid on;

%alpha_y
subplot(3,1,2)
plot(time_fuseon_phidgets_imu_1042_mag_data_raw,fuseon_phidgets_imu_1042_mag_data_raw.fieldvectory,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_mag_data_raw,fuseon_phidgets_imu_1044_mag_data_raw.fieldvectory,'r')
title('alpha_y')
xlabel('time (s)')
ylabel('angle (rad)')
grid on;

%alpha_z
subplot(3,1,3)
plot(time_fuseon_phidgets_imu_1042_mag_data_raw,fuseon_phidgets_imu_1042_mag_data_raw.fieldvectorz,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_mag_data_raw,fuseon_phidgets_imu_1044_mag_data_raw.fieldvectorz,'r')
title('alpha_z')
xlabel('time (s)')
ylabel('angle (rad)')
grid on;


% IMU

time_fuseon_phidgets_imu_1042_imu_data_raw=(fuseon_phidgets_imu_1042_imu_data_raw.VarName1-fuseon_phidgets_imu_1042_imu_data_raw.VarName1(1))/1e9;
time_fuseon_phidgets_imu_1044_imu_data_raw=(fuseon_phidgets_imu_1044_imu_data_raw.VarName1-fuseon_phidgets_imu_1044_imu_data_raw.VarName1(1))/1e9;


% Angular speed
figure
%wx
subplot(3,1,1)
plot(time_fuseon_phidgets_imu_1042_imu_data_raw,fuseon_phidgets_imu_1042_imu_data_raw.fieldangular_velocityx,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_imu_data_raw,fuseon_phidgets_imu_1044_imu_data_raw.fieldangular_velocityx,'r')
title('w_x')
xlabel('time (s)')
ylabel('rot speed (rad/s)')
grid on;

%wy
subplot(3,1,2)
plot(time_fuseon_phidgets_imu_1042_imu_data_raw,fuseon_phidgets_imu_1042_imu_data_raw.fieldangular_velocityy,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_imu_data_raw,fuseon_phidgets_imu_1044_imu_data_raw.fieldangular_velocityy,'r')
title('w_y')
xlabel('time (s)')
ylabel('rot speed (rad/s)')
grid on;

%wz
subplot(3,1,3)
plot(time_fuseon_phidgets_imu_1042_imu_data_raw,fuseon_phidgets_imu_1042_imu_data_raw.fieldangular_velocityz,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_imu_data_raw,fuseon_phidgets_imu_1044_imu_data_raw.fieldangular_velocityz,'r')
title('w_z')
xlabel('time (s)')
ylabel('rot speed (rad/s)')
grid on;


% Linear Acceleration
figure
%ax
subplot(3,1,1)
plot(time_fuseon_phidgets_imu_1042_imu_data_raw,fuseon_phidgets_imu_1042_imu_data_raw.fieldlinear_accelerationx,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_imu_data_raw,fuseon_phidgets_imu_1044_imu_data_raw.fieldlinear_accelerationx,'r')
title('a_x')
xlabel('time (s)')
ylabel('lin accel (m/s²)')
grid on;

%ay
subplot(3,1,2)
plot(time_fuseon_phidgets_imu_1042_imu_data_raw,fuseon_phidgets_imu_1042_imu_data_raw.fieldlinear_accelerationy,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_imu_data_raw,fuseon_phidgets_imu_1044_imu_data_raw.fieldlinear_accelerationy,'r')
title('a_y')
xlabel('time (s)')
ylabel('lin accel (m/s²)')
grid on;

%az
subplot(3,1,3)
plot(time_fuseon_phidgets_imu_1042_imu_data_raw,fuseon_phidgets_imu_1042_imu_data_raw.fieldlinear_accelerationz,'m')
hold on;
plot(time_fuseon_phidgets_imu_1044_imu_data_raw,fuseon_phidgets_imu_1044_imu_data_raw.fieldlinear_accelerationz,'r')
title('a_z')
xlabel('time (s)')
ylabel('lin accel (m/s²)')
grid on;
