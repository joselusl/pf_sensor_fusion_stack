#!/bin/bash

# VARS
ROS_BAG_PATH="$FUSEON_STACK/logs/session1/rosbag/session1_2015-08-05-16-24-49.bag"
OUTPUT_PATH="$FUSEON_STACK/logs/session1/csv/session1_2015-08-05-16-24-49"

# TOPICS
# /fuseon/hokuyo_lidar/scan
# /fuseon/hokuyo_lidar/urg_node/parameter_descriptions
# /fuseon/hokuyo_lidar/urg_node/parameter_updates
# /fuseon/phidgets_gps_1040/gps/nav_sat_fix
# /fuseon/phidgets_gps_1040/gps/nav_sat_status
# /fuseon/phidgets_gps_1040/time_reference
# /fuseon/phidgets_imu_1042/imu/data_raw
# /fuseon/phidgets_imu_1042/mag/data_raw
# /fuseon/phidgets_imu_1044/imu/data_raw
# /fuseon/phidgets_imu_1044/mag/data_raw
# /fuseon/usglobalsat_gps/nav_sat_fix
# /fuseon/usglobalsat_gps/nav_sat_vel
# /fuseon/usglobalsat_gps/time_reference



# Create dir of output
mkdir -p $OUTPUT_PATH

# TO CSV
# hokuyo_lidar
echo "hokuyo_lidar"
rostopic echo /fuseon/hokuyo_lidar/scan -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_hokuyo_lidar_scan.csv

rostopic echo /fuseon/hokuyo_lidar/urg_node/parameter_descriptions -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_hokuyo_lidar_urg_node_parameter_descriptions.csv

rostopic echo /fuseon/hokuyo_lidar/urg_node/parameter_updates -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_hokuyo_lidar_urg_node_parameter_updates.csv


# phidgets_gps_1040
echo "phidgets_gps_1040"
rostopic echo /fuseon/phidgets_gps_1040/gps/nav_sat_fix -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_phidgets_gps_1040_gps_nav_sat_fix.csv

rostopic echo /fuseon/phidgets_gps_1040/gps/nav_sat_status -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_phidgets_gps_1040_gps_nav_sat_status.csv

rostopic echo /fuseon/phidgets_gps_1040/time_reference -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_phidgets_gps_1040_time_reference.csv


# phidgets_imu_1042
echo "phidgets_imu_1042"
rostopic echo /fuseon/phidgets_imu_1042/imu/data_raw -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_phidgets_imu_1042_imu_data_raw.csv

rostopic echo /fuseon/phidgets_imu_1042/mag/data_raw -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_phidgets_imu_1042_mag_data_raw.csv


# phidgets_imu_1044
echo "phidgets_imu_1044"
rostopic echo /fuseon/phidgets_imu_1044/imu/data_raw -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_phidgets_imu_1044_imu_data_raw.csv

rostopic echo /fuseon/phidgets_imu_1044/mag/data_raw -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_phidgets_imu_1044_mag_data_raw.csv


# usglobalsat_gps
echo "usglobalsat_gps"
rostopic echo /fuseon/usglobalsat_gps/nav_sat_fix -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_usglobalsat_gps_nav_sat_fix.csv

rostopic echo /fuseon/usglobalsat_gps/nav_sat_vel -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_usglobalsat_gps_nav_sat_vel.csv

rostopic echo /fuseon/usglobalsat_gps/time_reference -b $ROS_BAG_PATH -p > $OUTPUT_PATH/fuseon_usglobalsat_gps_time_reference.csv



