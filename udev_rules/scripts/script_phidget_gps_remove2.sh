#!/bin/bash

echo 'Phidget GPS disconnected' >> '/home/joselusl/udev_test.txt'


cd /home/joselusl/workspace/ros/pf_sensor_fusion_catkin_ws
source devel/setup.bash
rosnode kill /PhidgetsGps
