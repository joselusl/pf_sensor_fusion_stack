#!/bin/bash

# To restart udev
# sudo restart udev

# To monitor udev
# udevadm monitor --environment --udev
# udevadm monitor

# input
action=$1

# Username - To Be Defined
username=joselusl

if [ "$action" = "add" ]
then
	cmd='/home/joselusl/script_phidget_gps_add.sh'
elif [ "$action" = "remove" ]
then
	cmd='/home/joselusl/script_phidget_gps_remove.sh'
fi

# Command
'/bin/su' $username -s '/bin/bash' -c $cmd &
exit
